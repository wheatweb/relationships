@foreach($navigations as $navigation)
<h1>{{ $navigation-> title }} nav</h1>
<p>Nav's Slug: {{ $navigation->slug }}</p>

@foreach($navigation->links as $link)
<h3>Link: {{ $link->title }}</h3>
<ul>
    <li>Link ID: {{ $link->id }}</li>
    <li>Slug: {{ $link->slug }}</li>
    <li>Page ID: {{ $link->page_id }}</li>
    <li>Parent ID: {{ $link->parent_id }}</li>
</ul>
@endforeach
@endforeach

<?php /*
@foreach($navTree as $tree)
@if(count($tree->children) > 0)
    <h3>{{ $tree->title }}</h3>
    <p>Link ID: {{ $tree->id }}</p>
    <p>Parent Link: {{ $tree->parent_id }}</p>
    <p>Page: {{ $tree->page->title }}</p>
    <ul>
    @foreach($tree->children as $subNav)
        <li>{{ $subNav->title }}</li>
        <p>Link ID: {{ $subNav->id }}</p>
        <p>Parent Link: {{ $subNav->parent_id }}</p>
        <p>Page: {{ $subNav->page->title }}</p>
    @endforeach
    </ul>
@else
    <h3>{{ $tree->title }}</h3>

    <p>Link ID: {{ $tree->id }}</p>
    <p>Parent Link: {{ $tree->parent_id }}</p>
    <p>Page: {{ $tree->page->title }}</p>
@endif
@endforeach
*/ ?>