@foreach($pages as $page)
    <h1>{{ $page->title }} page links</h1>
    <ul>
    @foreach($page->links as $link)
        <li>Title: {{ $link->title }}</li>
        <p>Slug: {{$link->slug}}</p>
    @endforeach
    </ul>
@endforeach
<?php /*
*/ ?>