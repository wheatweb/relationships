<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('index');
Route::get('/page/', 'PageController@pages')->name('pages');
Route::get('/page/{slug}', 'PageController@page')->name('slug');
Route::get('/link/{slug}', 'PageController@link')->name('slug');
Route::get('/navigation/', 'PageController@navigations')->name('navigation');
Route::get('/navigation/{nav}', 'PageController@navigation')->name('nav');

