<?php

use Illuminate\Database\Seeder;

class LinkSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('links')->truncate();

        DB::table('links')->insert([
            [
                'title' => 'Home',
                'slug' => '',
                'page_id' => 1,
                'parent_id' => 0
            ],
            [
                'title' => 'About Us',
                'slug' => 'about',
                'page_id' => 2,
                'parent_id' => 0
            ],
            [
                'title' => 'Laminates',
                'slug' => 'laminates',
                'page_id' => 3,
                'parent_id' => 0
            ],
            [
                'title' => 'Decorative Laminates',
                'slug' => 'decorative-laminates',
                'page_id' => 4,
                'parent_id' => 3
            ],
            [
                'title' => 'Metallic Laminates',
                'slug' => 'metallic-laminates',
                'page_id' => 5,
                'parent_id' => 3
            ],
            [
                'title' => 'Lamitech Laminates',
                'slug' => 'lamitech-laminates',
                'page_id' => 6,
                'parent_id' => 0
            ],
            [
                'title' => 'Home Laminates',
                'slug' => 'lamitech-laminates',
                'page_id' => 3,
                'parent_id' => 0
            ],
        ]);
    }
}
