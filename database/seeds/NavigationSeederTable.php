<?php

use Illuminate\Database\Seeder;

class NavigationSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('navigations')->truncate();

        DB::table('navigations')->insert([
            [
                'title' => 'Navigation',
                'slug' => 'topNav'
            ],
            [
                'title' => 'Catalog',
                'slug' => 'catNav'
            ]
        ]);
    }
}
