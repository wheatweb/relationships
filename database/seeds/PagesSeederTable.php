<?php

use Illuminate\Database\Seeder;

class PagesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->truncate();

        DB::table('pages')->insert([
            [
                'title' => 'Home',
                'slug' => '',
            ],
            [
                'title' => 'About Us',
                'slug' => 'about',
            ],
            [
                'title' => 'Laminates',
                'slug' => 'laminates',
            ],
            [
                'title' => 'Decorative Laminates',
                'slug' => 'decorative-laminates',
            ],
            [
                'title' => 'Metallic Laminates',
                'slug' => 'metallic-laminates',
            ],
            [
                'title' => 'Lamitech Laminates',
                'slug' => 'lamitech-laminates',
            ],
        ]);
    }
}
