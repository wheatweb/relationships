<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(LinkSeederTable::class);
        $this->call(NavigationSeederTable::class);
        $this->call(PagesSeederTable::class);
        $this->call(LinkNavigationSeederTable::class);
    }
}
