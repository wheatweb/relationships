<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    public function pages()
    {
        return $this->belongsTo(Page::class);
    }

/*
    public function navigation()
    {
        return $this->belongsTo(Navigation::class);
    }
*/

    public function navigations()
    {
        return $this->belongsToMany(Navigation::class);
    }

    public function children()
    {
        return $this->hasMany(Link::class, 'parent_id', 'id');
    }

    public function parentLink()
    {
        return $this->hasOne(Link::class, 'parent_id', 'id');
    }

    public static function tree($navId, $level = 3)
    {
      return static::with(implode('.', array_fill(0, $level, 'children')))->where([['parent_id', '=', 0],['navigation_id', '=', $navId]])->get();
    }

    public function navTree($navId, $level = 3)
    {
      DB::enableQueryLog();
      $tree = Link::with(implode('.', array_fill(0, $level, 'children')))->where([['parent_id', '=', 0],['navigation_id', '=', $navId]])->get();
//      dd($tree, $staticTree, DB::getQueryLog());

      return $tree;
    }
}
