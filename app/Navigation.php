<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Navigation extends Model
{
    public function links()
    {
        return $this->belongsToMany(Link::class);
    }
}
