<?php

namespace App\Http\Controllers;

use App\Link;
use App\Page;
use App\Navigation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    public function index()
    {
        DB::enableQueryLog();
        $pages = Page::all();
        //$pages = Page::with('link')->get();
//        dd(DB::getQueryLog(), $pages);

        return view('page', compact('pages'));
    }

    public function link($slug)
    {
        $link = Link::where('slug', $slug)->get();

        return view('link', compact('link'));
    }

    public function page($slug)
    {
        $pages = Page::with('links')->where('slug', $slug)->get(); //where('slug', $slug)->firstOrFail();

        return view('page', compact('pages'));
    }

    public function pages()
    {
        $pages = Page::with('links')->get();

        return view('page', compact('pages'));
    }

    public function navigations()
    {
        $navigations = Navigation::all();

        return view('navigation', compact('navigations'));
    }

    public function navigation($nav)
    {
        DB::enableQueryLog();
        $navigations = Navigation::with('links')->where('slug', $nav)->get();
        //$navigations = Navigation::has('links', '>', 1)->get();
        //$navigations = $this->buildNav($nav);
        //dd(DB::getQueryLog(), $navigations);

//        dd($navTree);

        return view('navigation', compact('navigations'));
    }

    public function buildNav($navSlug)
    {
        $level = 3;
        // Get the navigation links from the slug
        $nav = Navigation::with('links')->where('slug', $navSlug)->first();
        // Return the navigation's links and any links with children
        //return Link::with(implode('.', array_fill(0, $level, 'children')))->where([['parent_id', '=', 0]])->whereHas([['navigations', '=', $nav->id]])->get();;
        return Link::with(implode('.', array_fill(0, $level, 'children')))->where([['parent_id', '=', 0], ['navigations', '=', $nav->id]])->get();;
    }
}
